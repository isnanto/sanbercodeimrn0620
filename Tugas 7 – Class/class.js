//No. 1
class Animal {
    constructor(name) {
        this.name = name;
        this.legs = 4;
        this.cold_blooded = false;
    }
}

var sheep = new Animal("shaun");
console.log(sheep.name);
console.log(sheep.legs);
console.log(sheep.cold_blooded);

console.log("RELEASE 1")
class Ape extends Animal{
    constructor() {
        super().legs = 2;
    }
    yell() {
        console.log("Auooo")
    }
}

class Frog extends Animal{
    constructor() {
        super()
    }
    jump() {
        console.log("hop hop")
    }
} 

var sungokong = new Ape("kera sakti")
sungokong.yell()

var kodok = new Frog("buduk")
kodok.jump()

//No. 2
class Clock {
    constructor( temp ) {
      this.template = temp.template
      this.timer = undefined
    }
  
    render() {
      var date 	  = new Date();
  
      var hours = date.getHours();
      if (hours < 10) hours = '0' + hours;
  
      var mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;
  
      var secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
  
      var output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
      console.log(output);
    }
  
    stop() {
      clearInterval(this.timer);
    }
  
    start() {
      this.render()
      this.timer = setInterval( this.render.bind(this) , 1000);
  }
  }
  

  var clock = new Clock({template: 'h:m:s'});
  clock.start();
    
