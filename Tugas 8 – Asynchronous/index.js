// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
var waktu = 10000
var bukuke = 0
var bacabuku = function( waktu, bukus ) {
	if ( bukuke < bukus.length ) {
		readBooks( waktu, bukus[bukuke], function() {
			bacabuku(waktu, bukus)			
		})
		waktu -= bukus[bukuke].timeSpent
		bukuke++
	}
}

bacabuku(waktu, books)
