// No. 1 Range
function range(num1 = 0, num2 = 0) {
    var ar = []
    if (num1 == 0 || num2 == 0){
        ar.push(-1)
    }else {
        if (num1 < num2){  //para< < > param2 askending
            for (let index = num1; index <= num2;  index++) {
                ar.push(index)                
            }
        } else {  //
            for (let index = num1; index >= num2;  index--) {
                ar.push(index)                
            }
        }
    }
    
    return ar
}
console.log('Soal No. 1')
console.log('-----------------------------------')
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

//No. 2 
var rangeWithStep = function( num1=0, num2=0, step=1 ) {
	var ar = []
	
	if( num1 < num2 ) {		
		for (var a = num1; a <= num2; a += step) {
			ar.push(a) 
		}

	} else {
		for (var b = num1; b >= num2 ; b-=step) {
			ar.push(b)
		}
	}

	return ar
}

console.log('Soal No. 2')
console.log('-----------------------------------')
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

//No. 3
function sum(a,b,step=1) {
	var ar = rangeWithStep(a,b,step)	
	var total = 0
	for (var i = 0; i < ar.length; i++) {
		total += ar[i]
	}

	return total
}

console.log('Soal No. 3')
console.log('-----------------------------------')
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

//No. 4
function datahandling(ar) {
	var tampil = ""
	var item = ["Nomor ID      : ","Nama Lengkap  : ","TTL           : ", "Hobi          : "]

	for (var i = 0; i < ar.length; i++) {
	
		panjang = ar[i].length -1
		for (var x = 0; x < panjang ; x++) {
			if(x==3) {
				tampil += item[x] + ar[i][x+1] + '\n\n'
			} else if (x==2) {
				tampil += item[x] + ar[i][x] +' '+ ar[i][x+1] + '\n'
			} else {
				tampil += item[x] + ar[i][x] + '\n'
			}
		}
	
	}
	return tampil
}

var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ]

console.log('Soal No. 4')
console.log('-----------------------------------')
console.log( datahandling(input) )


//No. 5
var balikKata = function(kata) {
	var balik = ""
	for (var i = kata.length - 1; i >= 0 ; i--) {
		balik += kata[i]
	}
	return balik
}
console.log('Soal No. 5')
console.log('-----------------------------------')
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 
console.log()

//No. 6
var namaBln = function(bulan) {
	switch(bulan) {
	  case 1:   { bln = "Januari"; break; }
	  case 2:   { bln = "Februari"; break; }
	  case 3:   { bln = "Maret"; break; }
	  case 4:   { bln = "April"; break; }
	  case 5:   { bln = "Mei"; break; }
	  case 6:   { bln = "Juni"; break; }
	  case 7:   { bln = "Juli"; break; }
	  case 8:   { bln = "Agustus"; break; }
	  case 9:   { bln = "September"; break; }
	  case 10:  { bln = "Oktober"; break; }
	  case 11:  { bln = "November"; break; }
	  case 12:  { bln = "Desember"; break; }
	  default:  { }
	}

	return bln
}

var sortingArrayDescending = function(arr) {
	return arr.sort(function(a,b){return b-a})
}

var joinArrayToString = function(arr) {
	return arr.join("-")
}

var limiting = function(nama) {
	return nama.slice(0,15)
}

var dataHandling2 = function(data) {
	newdata = data
	tampilkan = ""

	newdata.splice(-1, 1) 
	newdata.splice(1, 2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung")  
	newdata.splice(4, 2, "Pria", "SMA Internasional Metro")  

	bln 		 = newdata[3].split("/")

	bulanNumeric = bln.splice(0, 3, parseInt(bln[0]), parseInt(bln[1]), parseInt(bln[2]) )

	namaBulan 	 = namaBln( +bln[1] )
	console.log(data)
	console.log(namaBulan)

	sorting 	 = sortingArrayDescending(bulanNumeric)
	console.log(sorting)

	joining 	 = joinArrayToString( newdata[3].split("/") )
	console.log(joining)

	limits 		 = limiting( newdata[1] )
	console.log(limits)	
 
	return ""
}

console.log('Soal No. 6')
console.log('-----------------------------------')
var data = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]  
console.log(dataHandling2(data) )
