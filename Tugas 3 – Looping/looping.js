//No. 1 Looping while
var k = 2
console.log('LOOPING PERTAMA')
while (  k <= 20) {
    console.log(k + ' - I love coding'  )
    k += 2
}

console.log('LOOPING KEDUA')
var k = 20
while (  k > 1) {
    console.log(k + ' - I will become a mobile developer')
    k -= 2
}

//No. 2. Looping menggunakan for
console.log('OUTPUT')
for (let index = 1; index <= 20; index++){
    var kata =''
    if (index % 2 == 0)  {
        kata = ' - Berkualitas'
    }
    else if (index % 3 == 0) {
        kata=' - I Love Coding'
    }    
    else {
        kata=' - Santai'
    }
    console.log(index + kata )   
}

//No. 3 Membuat persegi panjang
for (let index = 1; index < 5; index++) {
    console.log('#'.repeat(8))    
}

//No. 4. Membuat tangga
for (let index = 0; index < 8; index++) {
    console.log('#'.repeat(index))
}

//No. 5. Membuat papan catur
for (let index = 1; index <=8 ; index++) {
    if (index % 2 ==0){
        console.log(' #'.repeat(4))
    }
    else {
        console.log('# '.repeat(4))
    }    
}