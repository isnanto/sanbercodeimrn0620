//No. 1
function arrayToObject(arr) {
    // Code di sini
   obj = {} 
   tahunini = new Date().getFullYear()   
   if (arr.length == 0){
       console.log(obj)
       return
   }
   for (let i=0; i<arr.length; i++){
       tahun = arr[i][3]
       if (tahun == undefined || tahun > tahunini){
           umur= "Invalid Birth Year"
       }else{
           umur = tahunini - tahun
       }
       obj.firstName = arr[i][0]
       obj.lastName = arr[i][1]
       obj.gender = arr[i][2]
       obj.age = umur
       console.log(obj)
   }
}

// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]

//console.log(people.length)
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""

//No. 2
function shoppingTime(memberId, money) {
    desc = ""
    var obj = {}
    var min = 50000
    var daftar = [
                        [	'1820RzKrnWn08',  2475000,  [ 'Sepatu Stacattu', 'Baju Zoro', 'Baju H&N', 'Sweater Uniklooh', 'Casing Handphone'],0 ],
                        [	'82Ku8Ma742',  170000, [ 'Casing Handphone' ], 120000]
                      ]
    if( memberId === undefined || memberId === '' ) {
        desc = "Mohon maaf, toko X hanya berlaku untuk member saja"

    } else if( money < min ) {
        desc = "Mohon maaf, uang tidak cukup"

    } else {

        for (var i = 0; i < daftar.length; i++) {
            if ( memberId == daftar[i][0] ) {
                obj =  {
                            memberId : daftar[i][0],
                            money  : daftar[i][1],
                            listPurchased : daftar[i][2],
                            changeMoney : daftar[i][3]
                        }
            }
        }
        desc = obj

    }
    return desc
}

console.log( "No. 2" )
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

//No. 3
function naikAngkot(arrPenumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    rute = rute.sort(function(a,b) {return a < b ? -1 : 1})

    var output = [];
    var naik = 0;
    var tujuan = 0;
    var indexNaik = 0;
    var indexTujuan = 0;
    var totalPrice = 0;

    if (arrPenumpang !== undefined || arrPenumpang.length != 0) {
        for (let x = 0; x < arrPenumpang.length; x++) {
            naik = arrPenumpang[x][1];
            tujuan = arrPenumpang[x][2];
    
            for (let index = 0; index < rute.length; index++) {
                if (naik == rute[index]) {
                    indexNaik = index + 1;
                }
                if (tujuan == rute[index]) {
                    indexTujuan = index + 1;
                }
            }
            var listAngkot = {};
            totalPrice = (indexTujuan - indexNaik) * 2000;
            listAngkot.penumpang = arrPenumpang[x][0];
            listAngkot.naikDari = naik;
            listAngkot.tujuan = tujuan;
            listAngkot.bayar = totalPrice
            output.push(listAngkot);
        }
    }
    return output
}
console.log('No. 3')
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([]))


